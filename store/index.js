import Vuex from 'vuex'
import members from "../data/dummy.json";

const store = () => {
  return new Vuex.Store({
    state: {
      members
    }
  })
}

export default store
